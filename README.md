# LEIA-ME #

Este arquivo README documenta etapas do curso de EcmaScript 6 - ES6
School of net - https://www.schoolofnet.com/curso/frontend/javascript/es6-rev2/


### PROPÓSITO DO ES6? ###

* RESUMO RÁPIDO
* Uma nova forma de desenvolver e tirar vantagens de features que se aproximam mais do POO.
* Desenvolvido para ser o novo padrão de desenvolvimento nos browsers atuais.
* Pronto para ser usado em JS como Backend
* Version 0.0.1
* 
* [Official Site](https://www.ecma-international.org/publications/standards/Ecma-262.htm)
* https://en.wikipedia.org/wiki/ECMAScript
* https://en.wikipedia.org/wiki/Ecma_International


## INTRODUÇÃO (aulas 1 e 2)

### Como surgiu?

* Surgiu com NetScape na década de 90
* Javascript foi criado pela NetScape
* Até bem pouco tempo, utilizavamos em larga escala o ES5, que mais perdurou no navegadores.
* Na versão/publicação 6 do Javascript, em 2015, tornou-se conhecida a ES6(Referencia a versão de June2015)
* A sexta edição, inicialmente conhecida como ECMAScript 6 (ES6) e posteriormente renomeada para ECMAScript 2015 (ES2015) [10], adiciona nova sintaxe significativa para a escrita de aplicações complexas, incluindo classes e módulos, mas as define semanticamente nos mesmos termos do ECMAScript 5 strict. modo. Outros novos recursos incluem iteradores e / loops, geradores e expressões geradoras no estilo Python, funções de seta, dados binários, array tipados, collections (maps, conjuntos e mapas fracos), promises, aprimoramentos de números e de matemática, reflexion e proxies ( metaprogramação para objetos virtuais e wrappers). Como a primeira especificação "ECMAScript Harmony", também é conhecida como "ES6 Harmony".

### Compatiblidade dos Browsers (aula 3)

* https://kangax.github.io/compat-table/es6/
* Devido entusiamos e falta de compatibilidade dos browsers. No passado bem recente, existiam libs que convertiam o novo ES6 para ES5 (Transpilação). Tais como WebPack, BrowseFy, Beboo e outras.
* Já existe Javascript para Desktop. Electron

### VAR vs LET vs CONST (aula 4)

#### Problemas com Var
* Permite redeclarar. Eleva o contexto para alto nível.
* A declaração evade divisas do escopo do if/for etc. Se declarada dentro de um if, ela pode ser acessado fora, por exemplo.





### Como faço para configurar? ###

*

### Diretrizes de contribuição ###

* 
### Com quem eu falo? ###

* 